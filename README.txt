Connect2id Server SDK

Copyright (c) Connect2id Ltd., 2013 - 2014


README

Toolkit for developing Connect2id Server extensions.

1. Java Service Provider Interface (SPI) for sourcing OpenID Connect claims
   about a subject (end-user). Used by the Connect2id Server in its OpenID
   Connect Provider (OP) role to aggregate claims from one or more sources.

2. SPI for handling resource owner password credential grants (see RFC 6749,
   section 4.3). Used by the Connect2id Server to delegate processing of
   password grants to custom authorisation logic implementations.

3. SPI for handling client credential grants (see RFC 6749, section 4.4). Used
   by the Connect2id Server to delegate processing of client credentails grants 
   to custom authorisation logic implementations.


Download

Official releases of the Connect2id Server toolkit are pushed to Maven Central
under

    GroupId: com.nimbusds

    ArtifactId: c2id-server-sdk


These include the library’s source code, compiled JAR and JavaDocs.


To add the SDK to your Maven project use the following template:

    <dependency>
        <groupId>com.nimbusds</groupId>
        <artifactId>c2id-server-sdk</artifactId>
        <version>[version]</version>
    </dependency>

where [ version ] should match the expected by the particular Connect2id Server
version you're running.



Questions or comments? Email support@connect2id.com

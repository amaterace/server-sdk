package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.ClientID;


/**
 * Service Provider Interface (SPI) for handling token requests with an OAuth
 * 2.0 Client Credentials grant.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>OAuth 2.0 (RFC 6749), sections 1.3.4 and 4.4.
 * </ul>
 */
public interface ClientCredentialsGrantHandler extends GrantHandler {


	/**
	 * Handles a Client Credentials grant request. The client is
	 * confidential and always authenticated.
	 *
	 * @param scope          The requested scope, {@code null} if not
	 *                       specified.
	 * @param clientID       The client identifier. Not {@code null}.
	 * @param clientMetadata The OAuth 2.0 client metadata. Not
	 *                       {@code null}.
	 *
	 * @return The client credentials grant authorisation response.
	 *
	 * @throws GeneralException If the grant is denied, or another
	 *                          exception was encountered.
	 */
	public GrantAuthorization processGrant(final Scope scope,
					       final ClientID clientID,
					       final ClientMetadata clientMetadata)
		throws GeneralException;
}

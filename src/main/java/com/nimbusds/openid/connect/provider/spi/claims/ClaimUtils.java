package com.nimbusds.openid.connect.provider.spi.claims;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.nimbusds.langtag.LangTag;


/**
 * Claim utilities.
 */
public class ClaimUtils {


	/**
	 * Applies the specified language tags to a set of claims.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * Claims: "name", "given_name", "family_name"
	 * Language tags: "bg-BG", "en-US"
	 * Result: "name#bg-BG", "name#en-US",
	 *         "given_name#bg-BG", "given_name#en-US",
	 *         "family_name#bg-BG", "family_name#en-US"
	 * </pre>
	 *
	 * @param claims   The claims to apply the language tags to. Claims
	 *                 that already have a language will be returned
	 *                 unmodified. Must not be {@code null}.
	 * @param langTags The language tags to apply, {@code null} if not
	 *                 specified.
	 *
	 * @return The claims with applied language tags, or the original
	 *         claims if no language tags are specified.
	 */
	public static Set<String> applyLangTags(final Set<String> claims, final List<LangTag> langTags) {

		if (langTags == null || langTags.isEmpty())
			return claims;

		Set<String> out = new HashSet<>();

		for (String claim: claims) {

			for (LangTag tag: langTags) {

				if (claim.contains("#")) {
					out.add(claim);
				} else {
					out.add(claim + "#" + tag);
				}
			}
		}

		return out;
	}

	/**
	 * Prevents public instantiation.
	 */
	private ClaimUtils() {}
}
package com.nimbusds.openid.connect.provider.spi;


import java.io.InputStream;
import javax.servlet.ServletContext;


/**
 * Servlet-based context for the initialisation of SPI implementations.
 */
public class ServletInitContext implements InitContext {


	/**
	 * The servlet context.
	 */
	private final ServletContext servletContext;


	/**
	 * Creates a new servlet-based SPI initialisation context.
	 *
	 * @param servletContext The servlet context. Must not be {@code null}.
	 */
	public ServletInitContext(final ServletContext servletContext) {

		if (servletContext == null)
			throw new IllegalArgumentException("The servlet context must not be null");

		this.servletContext = servletContext;
	}


	/**
	 * Returns the servlet context.
	 *
	 * @return The servlet context.
	 */
	public ServletContext getServletContext() {

		return servletContext;
	}


	@Override
	public InputStream getResourceAsStream(final String path) {

		return servletContext.getResourceAsStream(path);
	}
}


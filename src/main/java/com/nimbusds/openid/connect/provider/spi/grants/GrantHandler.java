package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GrantType;

import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * OAuth 2.0 grant handler.
 */
public interface GrantHandler extends Lifecycle {


	/**
	 * Returns the handler grant type.
	 *
	 * @return The grant type;
	 */
	public GrantType getGrantType();
}

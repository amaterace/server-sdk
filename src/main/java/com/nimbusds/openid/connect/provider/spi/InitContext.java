package com.nimbusds.openid.connect.provider.spi;


import java.io.InputStream;


/**
 * Context for the initialisation of SPI implementations. Provides a method to
 * retrieve a configuration or another file from the web application.
 *
 * <p>Example use: Initialisation of OpenID Connect claims sources in the
 * Connect2id server.
 */
public interface InitContext {


	/**
	 * Returns the resource located at the named path as an input stream.
	 * Has the same behaviour as
	 * {@link javax.servlet.ServletContext#getResourceAsStream}.
	 *
	 * @param path The path to the resource, must be begin with a '/' and
	 *             is interpreted as relative to the web application root.
	 *             Must not be {@code null}.
	 *
	 * @return The resource as an input stream, or {@code null} if no
	 *         resource exists at the specified path.
	 */
	public InputStream getResourceAsStream(final String path);
}


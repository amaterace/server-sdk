package com.nimbusds.openid.connect.provider.spi.claims;


import java.util.List;
import java.util.Set;

import com.nimbusds.langtag.LangTag;

import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * Service Provider Interface (SPI) for sourcing OpenID Connect UserInfo and
 * other claims about a subject.
 */
public interface ClaimsSource extends Lifecycle {


	/**
	 * Returns the names of the supported OpenID Connect claims.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * name
	 * email
	 * email_verified
	 * </pre>
	 *
	 * @return The supported claim names. Should not include the reserved
	 *         {@code sub} (subject) claim name.
	 */
	public Set<String> supportedClaims();


	/**
	 * Requests claims for the specified subject.
	 *
	 * @param subject       The subject. Must not be {@code null}.
	 * @param claims        The names of the requested claims, with
	 *                      optional language tags. Must not be
	 *                      {@code null}.
	 * @param claimsLocales The preferred languages and scripts for the
	 *                      claims to return, {@code null} if not
	 *                      specified.
	 *
	 * @return The claims, {@code null} if the subject wasn't found or the
	 *         claims source is {@link #isEnabled disabled}.
	 *
	 * @throws Exception If retrieval of the claims failed.
	 */
	public UserInfo getClaims(final Subject subject,
				  final Set<String> claims,
				  final List<LangTag> claimsLocales)
		throws Exception;
}


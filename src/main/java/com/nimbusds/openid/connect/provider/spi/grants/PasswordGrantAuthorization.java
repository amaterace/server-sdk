package com.nimbusds.openid.connect.provider.spi.grants;


import java.util.*;

import net.minidev.json.JSONObject;

import com.nimbusds.langtag.LangTag;
import com.nimbusds.langtag.LangTagException;
import com.nimbusds.langtag.LangTagUtils;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;

import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;


/**
 * Authorisation response from a {@link PasswordGrantHandler}.
 *
 * <p>The minimum details it contains is the identifier of the authenticated
 * subject (end-user) and the authorised scope values. The other parameters are
 * optional or may have suitable defaults.
 */
public final class PasswordGrantAuthorization extends GrantAuthorization {


	/**
	 * The identifier of the authorised subject.
	 */
	private final Subject subject;


	/**
	 * The time of the subject authentication. If {@code null} it will be
	 * set to now. Applies only if an ID token is issued.
	 */
	private final Date authTime;


	/**
	 * The Authentication Context Class Reference (ACR), {@code null} if
	 * not specified. Applies only if an ID token is issued.
	 */
	private final ACR acr;


	/**
	 * The Authentication Methods Reference (AMR) list, {@code null} if not
	 * specified. Applies only if an ID token is issued.
	 */
	private final List<AMR> amrList;


	/**
	 * Controls the authorisation lifetime. {@code true} for a long-lived
	 * authorisation (implies persistence), {@code false} for a short-lived
	 * one.
	 */
	private final boolean longLived;


	/**
	 * Controls the refresh token issue. If {@code true} a refresh token
	 * must be issued (requires a long-lived authorisation), {@code false}
	 * if only an access token is issued.
	 */
	private final boolean issueRefreshToken;


	/**
	 * Controls the ID token issue. If {@code true} an ID token must be
	 * issued.
	 */
	private final boolean issueIDToken;


	/**
	 * Authorised OpenID Connect UserInfo claims, {@code null} if none.
	 */
	private final Set<String> claims;


	/**
	 * The preferred claims locales, {@code null} if not specified.
	 */
	private final List<LangTag> claimsLocales;


	/**
	 * Additional or preset claims to be included in the ID token,
	 * {@code null} if none.
	 */
	private final JSONObject presetIDTokenClaims;


	/**
	 * Additional or preset claims to be included in the UserInfo response,
	 * {@code null} if none.
	 */
	private final JSONObject presetUserInfoClaims;


	/**
	 * The preferred claims transport, {@code null} if not specified
	 * (implies UserInfo endpoint).
	 */
	private final ClaimsTransport claimsTransport;


	/**
	 * Creates a new OAuth 2.0 - only authorisation response from a
	 * {@link PasswordGrantHandler}.
	 *
	 * @param subject             The identifier of the authorised
	 *                            subject. Must not be {@code null}.
	 * @param scope               The authorised scope values. Must not be
	 *                            {@code null}.
	 * @param audList             Explicit list of audiences for the access
	 *                            token, {@code null} if not specified.
	 * @param longLived           Controls the authorisation lifetime.
	 *                            {@code true} for a long-lived
	 *                            authorisation (implies persistence),
	 *                            {@code false} for a short-lived one.
	 * @param accessTokenLifetime The access token lifetime, in seconds,
	 *                            zero if not specified.
	 * @param accessTokenEncoding The access token encoding, {@code null}
	 *                            if not specified.
	 * @param issueRefreshToken   Controls the refresh token issue. If
	 *                            {@code true} a refresh token must be
	 *                            issued (requires a long-lived
	 *                            authorisation), {@code false} if only an
	 *                            access token is issued.
	 */
	public PasswordGrantAuthorization(final Subject subject,
					  final Scope scope,
					  final List<Audience> audList,
					  final boolean longLived,
					  final long accessTokenLifetime,
					  final TokenEncoding accessTokenEncoding,
					  final boolean issueRefreshToken) {

		this (subject, null, null, null, scope, audList, longLived, accessTokenLifetime, accessTokenEncoding, issueRefreshToken,
			false, null, null, null, null, null);
	}


	/**
	 * Creates a new OpenID Connect / OAuth 2.0 authorisation response from
	 * a {@link PasswordGrantHandler}.
	 *
	 * @param subject              The identifier of the authorised
	 *                             subject. Must not be {@code null}.
	 * @param authTime             The time of the subject authentication.
	 *                             If {@code null} it will be set to now.
	 *                             Applies only if an ID token is issued.
	 * @param acr                  The Authentication Context Class
	 *                             Reference (ACR), {@code null} if not
	 *                             specified. Applies only if an ID token
	 *                             is issued.
	 * @param amrList              The Authentication Methods Reference
	 *                             (AMR) list, {@code null} if not
	 *                             specified. Applies only if an ID token
	 *                             is issued.
	 * @param scope                The authorised scope values. Must not be
	 *                             {@code null}.
	 * @param audList              Explicit list of audiences for the
	 *                             access token, {@code null} if not
	 *                             specified.
	 * @param longLived            Controls the authorisation lifetime.
	 *                             {@code true} for a long-lived
	 *                             authorisation (implies persistence),
	 *                             {@code false} for a short-lived one.
	 * @param accessTokenLifetime  The access token lifetime, in seconds,
	 *                             zero if not specified.
	 * @param accessTokenEncoding  The access token encoding, {@code null}
	 *                             if not specified.
	 * @param issueRefreshToken    Controls the refresh token issue. If
	 *                             {@code true} a refresh token must be
	 *                             issued (requires a long-lived
	 *                             authorisation), {@code false} if only an
	 *                             access token is issued.
	 * @param issueIDToken         Controls the ID token issue. If
	 *                             {@code true} an ID token must be issued.
	 * @param claims               Authorised OpenID Connect UserInfo
	 *                             claims, {@code null} if none.
	 * @param claimsLocales        The preferred claims locales,
	 *                             {@code null} if not specified.
	 * @param presetIDTokenClaims  Additional or preset claims to be
	 *                             included in the ID token, {@code null}
	 *                             if none.
	 * @param presetUserInfoClaims Additional or preset claims to be
	 *                             included in the UserInfo response,
	 *                             {@code null} if none.
	 * @param claimsTransport      The preferred claims transport,
	 *                             {@code null} if not specified (implies
	 *                             UserInfo endpoint).
	 */
	public PasswordGrantAuthorization(final Subject subject,
					  final Date authTime,
					  final ACR acr,
					  final List<AMR> amrList,
					  final Scope scope,
					  final List<Audience> audList,
					  final boolean longLived,
					  final long accessTokenLifetime,
					  final TokenEncoding accessTokenEncoding,
					  final boolean issueRefreshToken,
					  final boolean issueIDToken,
					  final Set<String> claims,
					  final List<LangTag> claimsLocales,
					  final JSONObject presetIDTokenClaims,
					  final JSONObject presetUserInfoClaims,
					  final ClaimsTransport claimsTransport) {

		super(scope, audList, accessTokenLifetime, accessTokenEncoding);

		if (subject == null)
			throw new IllegalArgumentException("The subject must not be null");

		this.subject = subject;

		this.authTime = authTime;
		this.acr = acr;
		this.amrList = amrList;
		this.longLived = longLived;
		this.issueRefreshToken = issueRefreshToken;
		this.issueIDToken = issueIDToken;
		this.claims = claims;
		this.claimsLocales = claimsLocales;
		this.presetIDTokenClaims = presetIDTokenClaims;
		this.presetUserInfoClaims = presetUserInfoClaims;
		this.claimsTransport = claimsTransport;
	}


	/**
	 * Returns the authorised subject.
	 *
	 * @return The identifier of the authorised subject.
	 */
	public Subject getSubject() {
		return subject;
	}


	/**
	 * Returns the time of the subject authentication.
	 *
	 * @return The time of the subject authentication. If {@code null} it
	 *         will be set to now. Applies only if an ID token is issued.
	 */
	public Date getAuthTime() {
		return authTime;
	}


	/**
	 * Returns the Authentication Context Class Reference (ACR).
	 *
	 * @return The Authentication Context Class Reference (ACR),
	 *         {@code null} if not specified. Applies only if an ID token
	 *         is issued.
	 */
	public ACR getACR() {
		return acr;
	}


	/**
	 * Returns The Authentication Methods Reference (AMR) list.
	 *
	 * @return The Authentication Methods Reference (AMR) list,
	 *         {@code null} if not specified. Applies only if an ID token
	 *         is issued.
	 */
	public List<AMR> getAMRList() {
		return amrList;
	}


	/**
	 * Returns the authorisation lifetime.
	 *
	 * @return {@code true} for a long-lived authorisation (implies
	 *         persistence), {@code false} for a short-lived one.
	 */
	public boolean isLongLived() {
		return longLived;
	}


	/**
	 * Returns the refresh token issue policy.
	 *
	 * @return {@code true} if refresh token issue is allowed (requires a
	 *         long-lived authorisation), else not.
	 */
	public boolean allowsRefreshTokenIssue() {
		return issueRefreshToken;
	}


	/**
	 * Returns the ID token issue policy.
	 *
	 * @return {@code true} to issue an ID token, else not.
	 */
	public boolean issueIDToken() {
		return issueIDToken;
	}


	/**
	 * Returns the authorised OpenID Connect UserInfo claims.
	 *
	 * @return The authorised OpenID Connect UserInfo claims, {@code null}
	 *         if none.
	 */
	public Set<String> getClaims() {
		return claims;
	}


	/**
	 * Returns the preferred OpenID Connect claims locales.
	 *
	 * @return The preferred OpenID Connect claims locales, {@code null} if
	 *         not specified.
	 */
	public List<LangTag> getClaimsLocales() {

		return claimsLocales;
	}


	/**
	 * Returns the additional or preset claims to be included in the ID
	 * token.
	 *
	 * @return The additional or preset claims to be included in the ID
	 *         token, {@code null} if none.
	 */
	public JSONObject getPresetIDTokenClaims() {
		return presetIDTokenClaims;
	}


	/**
	 * Returns the additional or preset claims to be included in the
	 * UserInfo response.
	 *
	 * @return The additional or preset claims to be included in the
	 *         UserInfo response, {@code null} if none.
	 */
	public JSONObject getPresetUserInfoClaims() {
		return presetUserInfoClaims;
	}


	/**
	 * Returns the preferred claims transport.
	 *
	 * @return The preferred claims transport, {@code null} if not
	 *         specified (implies UserInfo endpoint).
	 */
	public ClaimsTransport getClaimsTransport() {
		return claimsTransport;
	}


	/**
	 * Returns a JSON object representation of this authorisation response.
	 *
	 * @return The JSON object representation.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = super.toJSONObject();

		o.put("sub", subject.getValue());

		if (authTime != null) {
			o.put("auth_time", authTime.getTime() / 1000l);
		}

		if (acr != null) {
			o.put("acr", acr.getValue());
		}

		if (amrList != null) {

			List<String> sl = new ArrayList<>(amrList.size());

			for (AMR amr: amrList) {
				sl.add(amr.getValue());
			}

			o.put("amr", sl);
		}

		o.put("long_lived", longLived);

		o.put("issue_refresh_token", issueRefreshToken);

		o.put("issue_id_token", issueIDToken);

		if (claims != null) {
			o.put("claims", new ArrayList<>(claims));
		}

		if (claimsLocales != null) {

			o.put("claims_locales", LangTagUtils.toStringList(claimsLocales));
		}

		if (presetIDTokenClaims != null || presetUserInfoClaims != null) {

			JSONObject presetClaims = new JSONObject();

			if (presetIDTokenClaims != null) {
				presetClaims.put("id_token", presetIDTokenClaims);
			}

			if (presetUserInfoClaims != null) {
				presetClaims.put("userinfo", presetUserInfoClaims);
			}

			o.put("preset_claims", presetClaims);
		}

		if (claimsTransport != null) {
			o.put("claims_transport", claimsTransport);
		}

		return o;
	}


	/**
	 * Parses an authorisation response from the specified JSON object
	 * representation.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The authorisation response.
	 */
	public static PasswordGrantAuthorization parse(final JSONObject jsonObject)
		throws ParseException {

		GrantAuthorization basicAuthz = GrantAuthorization.parse(jsonObject);

		Subject sub = new Subject(JSONObjectUtils.getString(jsonObject, "sub"));

		Date authTime = null;

		if (jsonObject.containsKey("auth_time")) {
			authTime = new Date(JSONObjectUtils.getLong(jsonObject, "auth_time") * 1000l);
		}

		ACR acr = null;

		if (jsonObject.containsKey("acr")) {
			acr = new ACR(JSONObjectUtils.getString(jsonObject, "acr"));
		}

		List<AMR> amrList = null;

		if (jsonObject.containsKey("amr")) {
			String[] sa = JSONObjectUtils.getStringArray(jsonObject, "amr");
			amrList = new ArrayList<>(sa.length);
			for (String s: sa) {
				amrList.add(new AMR(s));
			}
		}

		boolean longLived = false;

		if (jsonObject.containsKey("long_lived")) {
			longLived = JSONObjectUtils.getBoolean(jsonObject, "long_lived");
		}

		boolean issueRefreshToken = false;

		if (jsonObject.containsKey("issue_refresh_token")) {
			issueRefreshToken = JSONObjectUtils.getBoolean(jsonObject, "issue_refresh_token");
		}

		boolean issueIDToken = false;

		if (jsonObject.containsKey("issue_id_token")) {
			issueIDToken = JSONObjectUtils.getBoolean(jsonObject, "issue_id_token");
		}

		Set<String> claims = null;

		if (jsonObject.containsKey("claims")) {
			claims = new HashSet<>(Arrays.asList(JSONObjectUtils.getStringArray(jsonObject, "claims")));
		}

		List<LangTag> claimsLocales = null;

		if (JSONObjectUtils.containsKey(jsonObject, "claims_locales")) {

			try {
				claimsLocales = LangTagUtils.parseLangTagList(JSONObjectUtils.getStringArray(jsonObject, "claims_locales"));

			} catch (LangTagException e) {

				throw new ParseException("Invalid claims locales value: " + e.getMessage(), e);
			}
		}

		JSONObject presetIDTokenClaims = null;
		JSONObject presetUserInfoClaims = null;

		if (jsonObject.containsKey("preset_claims")) {

			JSONObject presetClaims = JSONObjectUtils.getJSONObject(jsonObject, "preset_claims");

			if (presetClaims.containsKey("id_token")) {
				presetIDTokenClaims = JSONObjectUtils.getJSONObject(presetClaims, "id_token");
			}

			if (presetClaims.containsKey("userinfo")) {
				presetUserInfoClaims = JSONObjectUtils.getJSONObject(presetClaims, "userinfo");
			}
		}

		ClaimsTransport claimsTransport = null;

		if (jsonObject.containsKey("claims_transport")) {
			String c = JSONObjectUtils.getString(jsonObject, "claims_transport");

			try {
				claimsTransport = ClaimsTransport.valueOf(c.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new ParseException("Invalid claims transport");
			}
		}

		return new PasswordGrantAuthorization(
			sub, authTime, acr, amrList,
			basicAuthz.getScope(),
			basicAuthz.getAudience(),
			longLived,
			basicAuthz.getAccessTokenLifetime(),
			basicAuthz.getAccessTokenEncoding(),
			issueRefreshToken,
			issueIDToken, claims, claimsLocales, presetIDTokenClaims, presetUserInfoClaims, claimsTransport);
	}
}

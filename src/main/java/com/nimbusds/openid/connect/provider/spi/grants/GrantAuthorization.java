package com.nimbusds.openid.connect.provider.spi.grants;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


/**
 * Authorisation response from a grant handler.
 */
public class GrantAuthorization {


	/**
	 * The authorised scope values.
	 */
	private final Scope scope;


	/**
	 * Explicit list of audiences for the access token, {@code null} if
	 * none.
	 */
	private final List<Audience> audList;


	/**
	 * The access token lifetime, in seconds, zero if not specified.
	 */
	private final long accessTokenLifetime;


	/**
	 * The access token encoding, {@code null} if not specified.
	 */
	private final TokenEncoding accessTokenEncoding;


	/**
	 * Creates a new authorisation response from a grant handler.
	 *
	 * @param scope               The authorised scope values. Must not be
	 *                            {@code null}.
	 * @param audList             Explicit list of audiences for the access
	 *                            token, {@code null} if not specified.
	 * @param accessTokenLifetime The access token lifetime, in seconds,
	 *                            zero if not specified.
	 * @param accessTokenEncoding The access token encoding, {@code null}
	 *                            if not specified.
	 */
	public GrantAuthorization(final Scope scope,
				  final List<Audience> audList,
				  final long accessTokenLifetime,
				  final TokenEncoding accessTokenEncoding) {

		if (scope == null)
			throw new IllegalArgumentException("The scope must not be null");

		this.scope = scope;

		this.audList = audList;
		this.accessTokenLifetime = accessTokenLifetime;
		this.accessTokenEncoding = accessTokenEncoding;
	}


	/**
	 * Returns the authorised scope values.
	 *
	 * @return The authorised scope values.
	 */
	public Scope getScope() {
		return scope;
	}


	/**
	 * Returns the explicit list of audiences for the access token.
	 *
	 * @return The explicit list of audiences for the access token,
	 *         {@code null} if not specified.
	 */
	public List<Audience> getAudience() {
		return audList;
	}


	/**
	 * Returns the access token lifetime.
	 *
	 * @return The access token lifetime, in seconds, zero if not
	 *         specified.
	 */
	public long getAccessTokenLifetime() {
		return accessTokenLifetime;
	}


	/**
	 * Returns the access token encoding.
	 *
	 * @return The access token encoding, {@code null} if not specified.
	 */
	public TokenEncoding getAccessTokenEncoding() {
		return accessTokenEncoding;
	}


	/**
	 * Returns a JSON object representation of this authorisation response.
	 *
	 * @return The JSON object representation.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = new JSONObject();

		o.put("scope", scope.toStringList());

		if (audList != null) {

			List<String> sl = new ArrayList<>(audList.size());

			for (Audience aud: audList) {
				sl.add(aud.getValue());
			}

			o.put("audience", sl);
		}

		if (accessTokenLifetime > 0l || accessTokenEncoding != null) {

			JSONObject at = new JSONObject();

			if (accessTokenLifetime > 0l) {
				at.put("lifetime", accessTokenLifetime);
			}

			if (accessTokenEncoding != null) {
				at.put("encoding", accessTokenEncoding.toString());
			}

			o.put("access_token", at);
		}

		return o;
	}


	/**
	 * Parses an authorisation response from the specified JSON object
	 * representation.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The authorisation response.
	 */
	public static GrantAuthorization parse(final JSONObject jsonObject)
		throws ParseException {

		Scope scope = Scope.parse(Arrays.asList(JSONObjectUtils.getStringArray(jsonObject, "scope")));

		List<Audience> audList = null;

		if (jsonObject.containsKey("audience")) {
			String[] sa = JSONObjectUtils.getStringArray(jsonObject, "audience");
			audList = new ArrayList<>(sa.length);
			for (String s: sa) {
				audList.add(new Audience(s));
			}
		}

		long accessTokenLifetime = 0l;
		TokenEncoding accessTokenEncoding = null;

		if (jsonObject.containsKey("access_token")) {

			JSONObject at = JSONObjectUtils.getJSONObject(jsonObject, "access_token");

			if (at.containsKey("lifetime")) {
				accessTokenLifetime = JSONObjectUtils.getLong(at, "lifetime");
			}

			if (at.containsKey("encoding")) {
				String c = JSONObjectUtils.getString(at, "encoding");

				try {
					accessTokenEncoding = TokenEncoding.valueOf(c.toUpperCase());
				} catch (IllegalArgumentException e) {
					throw new ParseException("Invalid access token encoding");
				}
			}
		}

		return new GrantAuthorization(scope, audList, accessTokenLifetime, accessTokenEncoding);
	}
}

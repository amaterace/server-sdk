package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.ResourceOwnerPasswordCredentialsGrant;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;

import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


/**
 * Service Provider Interface (SPI) for handling token requests with an OAuth
 * 2.0 Resource Owner Password Credentials grant.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>OAuth 2.0 (RFC 6749), sections 1.3.3 and 4.3.
 * </ul>
 */
public interface PasswordGrantHandler extends GrantHandler {


	/**
	 * Handles a Resource Owner Password Credentials grant request.
	 *
	 * @param grant              The Resource Owner Password Credentials
	 *                           grant. Not {@code null}.
	 * @param scope              The requested scope, {@code null} if not
	 *                           specified.
	 * @param clientID           The client identifier. Not {@code null}.
	 * @param confidentialClient {@code true} if the client is confidential
	 *                           and has been authenticated, else
	 *                           {@code false}.
	 * @param clientMetadata     The OpenID Connect client metadata. Not
	 *                           {@code null}.
	 *
	 * @return The password grant authorisation response.
	 *
	 * @throws GeneralException If the grant is denied, or another
	 *                          exception was encountered.
	 */
	public PasswordGrantAuthorization processGrant(final ResourceOwnerPasswordCredentialsGrant grant,
						       final Scope scope,
						       final ClientID clientID,
						       final boolean confidentialClient,
						       final OIDCClientMetadata clientMetadata)
		throws GeneralException;
}


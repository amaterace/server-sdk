/**
 * OpenID Connect claims source SPI.
 */
package com.nimbusds.openid.connect.provider.spi.claims;

package com.nimbusds.openid.connect.provider.spi.grants;


import java.io.InputStream;

import junit.framework.TestCase;

import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;

import com.nimbusds.openid.connect.provider.spi.InitContext;


/**
 * Tests the client credentials grant handler interface.
 */
public class ClientCredentialsGrantHandlerTest extends TestCase {


	static class ExampleHandler implements ClientCredentialsGrantHandler {


		@Override
		public void init(final InitContext initContext)
			throws Exception {

		}


		@Override
		public boolean isEnabled() {

			return true;
		}


		@Override
		public GrantType getGrantType() {

			return GrantType.CLIENT_CREDENTIALS;
		}


		@Override
		public GrantAuthorization processGrant(final Scope scope,
						       final ClientID clientID,
						       final ClientMetadata clientMetadata)
			throws GeneralException {

			Scope allowedScopeValues = Scope.parse("read write");

			Scope requestedScope = scope;

			if (requestedScope == null) {
				requestedScope = allowedScopeValues;
			} else {
				requestedScope.retainAll(allowedScopeValues);
			}

			return new GrantAuthorization(requestedScope, null, 600l, TokenEncoding.SELF_CONTAINED);
		}


		@Override
		public void shutdown()
			throws Exception {

		}
	}


	public void testExampleHandler()
		throws Exception {

		ClientCredentialsGrantHandler handler = new ExampleHandler();
		handler.init(new InitContext() {
			@Override
			public InputStream getResourceAsStream(String path) {
				return null;
			}
		});
		assertEquals(GrantType.CLIENT_CREDENTIALS, handler.getGrantType());

		ClientID clientID = new ClientID("123");
		ClientMetadata clientMetadata = new ClientMetadata();

		GrantAuthorization authz = handler.processGrant(
			Scope.parse("read write admin"),
			clientID,
			clientMetadata);

		assertTrue(Scope.parse("read write").containsAll(authz.getScope()));
		assertEquals(2, authz.getScope().size());
		assertNull(authz.getAudience());
		assertEquals(600l, authz.getAccessTokenLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenEncoding());
	}
}


package com.nimbusds.openid.connect.provider.spi.grants;


import java.io.InputStream;

import junit.framework.TestCase;

import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;

import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;

import com.nimbusds.openid.connect.provider.spi.InitContext;


/**
 * Tests the password grant handler interface.
 */
public class PasswordGrantHandlerTest extends TestCase {


	static class ExampleHandler implements PasswordGrantHandler {


		@Override
		public void init(final InitContext initContext)
			throws Exception {

		}


		@Override
		public boolean isEnabled() {

			return true;
		}





		@Override
		public GrantType getGrantType() {

			return GrantType.PASSWORD;
		}


		@Override
		public PasswordGrantAuthorization processGrant(final ResourceOwnerPasswordCredentialsGrant grant,
							       final Scope scope,
							       final ClientID clientID,
							       final boolean confidentialClient,
							       final OIDCClientMetadata clientMetadata)
			throws GeneralException {

			if (grant.getUsername().equals("alice") && grant.getPassword().equals(new Secret("secret"))) {
				// Good credentials
			} else {
				// Bad credentials
				throw new GeneralException("Invalid user credentials", OAuth2Error.INVALID_GRANT);
			}

			Scope allowedScopeValuesForAlice = Scope.parse("openid email profile offline_access");

			Scope requestedScope = scope;

			if (requestedScope == null) {
				requestedScope = allowedScopeValuesForAlice;
			} else {
				requestedScope.retainAll(allowedScopeValuesForAlice);
			}

			return new PasswordGrantAuthorization(
				new Subject("alice"),
				requestedScope,
				null,
				false,
				3600l,
				TokenEncoding.SELF_CONTAINED,
				false);
		}


		@Override
		public void shutdown()
			throws Exception {

		}
	}


	public void testExampleHandler()
		throws Exception {

		PasswordGrantHandler handler = new ExampleHandler();
		handler.init(new InitContext() {
			@Override
			public InputStream getResourceAsStream(String path) {
				return null;
			}
		});
		assertEquals(GrantType.PASSWORD, handler.getGrantType());

		ResourceOwnerPasswordCredentialsGrant grant = new ResourceOwnerPasswordCredentialsGrant(
			"alice", new Secret("secret"));

		ClientID clientID = new ClientID("123");
		OIDCClientMetadata clientMetadata = new OIDCClientMetadata();

		PasswordGrantAuthorization authz = handler.processGrant(
			grant,
			Scope.parse("openid email admin"),
			clientID,
			true,
			clientMetadata);

		assertEquals("alice", authz.getSubject().getValue());
		assertTrue(Scope.parse("openid email").containsAll(authz.getScope()));
		assertEquals(2, authz.getScope().size());
	}
}


package com.nimbusds.openid.connect.provider.spi.grants;


import junit.framework.TestCase;

import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


/**
 * Tests the grant authorisation class.
 */
public class GrantAuthorizationTest extends TestCase {


	public void testConstructor()
		throws Exception {

		Scope scope = new Scope("openid", "email");

		GrantAuthorization authz = new GrantAuthorization(scope, null, 0l, null);

		assertTrue(scope.equals(authz.getScope()));
		assertNull(authz.getAudience());
		assertEquals(0l, authz.getAccessTokenLifetime());
		assertNull(authz.getAccessTokenEncoding());

		String json = authz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parseJSONObject(json);

		authz = GrantAuthorization.parse(jsonObject);

		assertTrue(scope.equals(authz.getScope()));
		assertNull(authz.getAudience());
		assertEquals(0l, authz.getAccessTokenLifetime());
		assertNull(authz.getAccessTokenEncoding());
	}


	public void testConstructorIllegalArgumentException() {

		try {
			Scope scope = null;
			new GrantAuthorization(scope, null, 0l, null);
			fail();
		} catch (IllegalArgumentException e) {
			// ok
		}
	}
}

package com.nimbusds.openid.connect.provider.spi.grants;


import java.util.*;

import junit.framework.TestCase;

import net.minidev.json.JSONObject;

import com.nimbusds.langtag.LangTag;
import com.nimbusds.langtag.LangTagUtils;

import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;

import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;


/**
 * Tests the password grant authorisation class.
 */
public class PasswordGrantAuthorizationTest extends TestCase {


	public void testOAuthConstructorMinimal()
		throws Exception {

		Subject sub = new Subject("alice");
		Scope scope = new Scope("openid", "email");

		PasswordGrantAuthorization authz = new PasswordGrantAuthorization(sub, scope, null, true, 0l, null, true);

		assertEquals(sub.getValue(), authz.getSubject().getValue());
		assertTrue(scope.equals(authz.getScope()));
		assertNull(authz.getAudience());
		assertTrue(authz.isLongLived());
		assertEquals(0l, authz.getAccessTokenLifetime());
		assertNull(authz.getAccessTokenEncoding());
		assertTrue(authz.allowsRefreshTokenIssue());

		String json = authz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parseJSONObject(json);

		authz = PasswordGrantAuthorization.parse(jsonObject);

		assertEquals(sub.getValue(), authz.getSubject().getValue());
		assertTrue(scope.equals(authz.getScope()));
		assertNull(authz.getAudience());
		assertTrue(authz.isLongLived());
		assertEquals(0l, authz.getAccessTokenLifetime());
		assertNull(authz.getAccessTokenEncoding());
		assertTrue(authz.allowsRefreshTokenIssue());
	}


	public void testOAuthConstructorFull()
		throws Exception {

		Subject sub = new Subject("alice");
		Scope scope = new Scope("openid", "email");
		List<Audience> aud = Arrays.asList(new Audience("a"), new Audience("b"));
		TokenEncoding enc = TokenEncoding.SELF_CONTAINED;

		PasswordGrantAuthorization authz = new PasswordGrantAuthorization(sub, scope, aud, false, 0l, enc, false);

		assertEquals(sub.getValue(), authz.getSubject().getValue());
		assertTrue(scope.equals(authz.getScope()));
		assertEquals("a", authz.getAudience().get(0).getValue());
		assertEquals("b", authz.getAudience().get(1).getValue());
		assertEquals(2, authz.getAudience().size());
		assertFalse(authz.isLongLived());
		assertEquals(0l, authz.getAccessTokenLifetime());
		assertEquals(enc, authz.getAccessTokenEncoding());
		assertFalse(authz.allowsRefreshTokenIssue());

		String json = authz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parseJSONObject(json);

		authz = PasswordGrantAuthorization.parse(jsonObject);

		assertEquals(sub.getValue(), authz.getSubject().getValue());
		assertTrue(scope.equals(authz.getScope()));
		assertEquals("a", authz.getAudience().get(0).getValue());
		assertEquals("b", authz.getAudience().get(1).getValue());
		assertEquals(2, authz.getAudience().size());
		assertFalse(authz.isLongLived());
		assertEquals(0l, authz.getAccessTokenLifetime());
		assertEquals(enc, authz.getAccessTokenEncoding());
		assertFalse(authz.allowsRefreshTokenIssue());
	}


	public void testOIDCConstructor()
		throws Exception {

		Subject sub = new Subject("alice");
		Date authTime = new Date(123l * 1000l);
		ACR acr = new ACR("urn:c2id:acr:strong-auth");
		List<AMR> amrList = Arrays.asList(new AMR("ldap"), new AMR("token"));
		Scope scope = new Scope("openid", "email");
		List<Audience> aud = Arrays.asList(new Audience("a"), new Audience("b"));
		long atl = 3600l;
		TokenEncoding enc = TokenEncoding.SELF_CONTAINED;
		Set<String> claims = new HashSet<>(Arrays.asList("openid", "email", "email_verified"));
		List<LangTag> claimsLocales = LangTagUtils.parseLangTagList("en", "es");
		JSONObject presetIDTokenClaims = new JSONObject();
		presetIDTokenClaims.put("group", "admin");
		JSONObject presetUserInfoClaims = new JSONObject();
		presetUserInfoClaims.put("office", "15");
		ClaimsTransport claimsTransport = ClaimsTransport.USERINFO;

		PasswordGrantAuthorization authz = new PasswordGrantAuthorization(
			sub, authTime, acr, amrList, scope, aud, true, atl, enc, true,
			true, claims, claimsLocales, presetIDTokenClaims, presetUserInfoClaims, claimsTransport);

		assertEquals(sub.getValue(), authz.getSubject().getValue());
		assertEquals(authTime.getTime(), authz.getAuthTime().getTime());
		assertEquals(acr.getValue(), authz.getACR().getValue());
		assertEquals(amrList.get(0).getValue(), authz.getAMRList().get(0).getValue());
		assertEquals(amrList.get(1).getValue(), authz.getAMRList().get(1).getValue());
		assertEquals(amrList.size(), authz.getAMRList().size());
		assertTrue(scope.equals(authz.getScope()));
		assertEquals(aud.get(0).getValue(), authz.getAudience().get(0).getValue());
		assertEquals(aud.get(1).getValue(), authz.getAudience().get(1).getValue());
		assertEquals(aud.size(), authz.getAudience().size());
		assertTrue(authz.isLongLived());
		assertEquals(atl, authz.getAccessTokenLifetime());
		assertEquals(enc, authz.getAccessTokenEncoding());
		assertTrue(authz.allowsRefreshTokenIssue());
		assertTrue(authz.issueIDToken());
		assertTrue(claims.containsAll(authz.getClaims()));
		assertEquals(claims.size(), authz.getClaims().size());
		assertTrue(claimsLocales.containsAll(authz.getClaimsLocales()));
		assertEquals(claimsLocales.size(), authz.getClaimsLocales().size());
		assertEquals("admin", (String)authz.getPresetIDTokenClaims().get("group"));
		assertEquals(1, authz.getPresetIDTokenClaims().size());
		assertEquals("15", (String)authz.getPresetUserInfoClaims().get("office"));
		assertEquals(1, authz.getPresetUserInfoClaims().size());

		String json = authz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parseJSONObject(json);

		authz = PasswordGrantAuthorization.parse(jsonObject);

		assertEquals(sub.getValue(), authz.getSubject().getValue());
		assertEquals(authTime.getTime(), authz.getAuthTime().getTime());
		assertEquals(acr.getValue(), authz.getACR().getValue());
		assertEquals(amrList.get(0).getValue(), authz.getAMRList().get(0).getValue());
		assertEquals(amrList.get(1).getValue(), authz.getAMRList().get(1).getValue());
		assertEquals(amrList.size(), authz.getAMRList().size());
		assertTrue(scope.equals(authz.getScope()));
		assertEquals(aud.get(0).getValue(), authz.getAudience().get(0).getValue());
		assertEquals(aud.get(1).getValue(), authz.getAudience().get(1).getValue());
		assertEquals(aud.size(), authz.getAudience().size());
		assertTrue(authz.isLongLived());
		assertEquals(atl, authz.getAccessTokenLifetime());
		assertEquals(enc, authz.getAccessTokenEncoding());
		assertTrue(authz.allowsRefreshTokenIssue());
		assertTrue(authz.issueIDToken());
		assertTrue(claims.containsAll(authz.getClaims()));
		assertEquals(claims.size(), authz.getClaims().size());
		assertTrue(claimsLocales.containsAll(authz.getClaimsLocales()));
		assertEquals(claimsLocales.size(), authz.getClaimsLocales().size());
		assertEquals("admin", (String)authz.getPresetIDTokenClaims().get("group"));
		assertEquals(1, authz.getPresetIDTokenClaims().size());
		assertEquals("15", (String)authz.getPresetUserInfoClaims().get("office"));
		assertEquals(1, authz.getPresetUserInfoClaims().size());
	}


	public void testConstructorIllegalArgumentException() {

		try {
			Subject sub = new Subject("alice");
			Scope scope = null;
			new PasswordGrantAuthorization(sub, scope, null, false, 0l, null, false);
			fail();
		} catch (IllegalArgumentException e) {
			// ok
		}


		try {
			Subject sub = null;
			Scope scope = new Scope("openid", "email");
			new PasswordGrantAuthorization(sub, scope, null, false, 0l, null, false);
			fail();
		} catch (IllegalArgumentException e) {
			// ok
		}
	}


	public void testParseMinimal()
		throws Exception {

		JSONObject o = new JSONObject();
		o.put("scope", Arrays.asList("openid", "email"));
		o.put("sub", "alice");

		String json = o.toJSONString();

		PasswordGrantAuthorization authz = PasswordGrantAuthorization.parse(JSONObjectUtils.parseJSONObject(json));

		assertTrue(Scope.parse("openid email").containsAll(authz.getScope()));
		assertEquals(2, authz.getScope().size());
		assertEquals("alice", authz.getSubject().getValue());

		assertNull(authz.getAudience());
		assertEquals(0l, authz.getAccessTokenLifetime());
		assertNull(authz.getAccessTokenEncoding());
		assertNull(authz.getAuthTime());
		assertNull(authz.getACR());
		assertNull(authz.getAMRList());
		assertFalse(authz.isLongLived());
		assertFalse(authz.allowsRefreshTokenIssue());
		assertFalse(authz.issueIDToken());
		assertNull(authz.getClaims());
		assertNull(authz.getClaimsLocales());
		assertNull(authz.getPresetIDTokenClaims());
		assertNull(authz.getPresetUserInfoClaims());
		assertNull(authz.getClaimsTransport());
	}
}


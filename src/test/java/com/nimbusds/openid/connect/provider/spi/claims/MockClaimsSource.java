package com.nimbusds.openid.connect.provider.spi.claims;


import java.util.*;

import javax.mail.internet.InternetAddress;

import com.nimbusds.langtag.LangTag;

import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

import com.nimbusds.openid.connect.provider.spi.InitContext;


/**
 * Mock claims source.
 */
public class MockClaimsSource implements ClaimsSource {


	private boolean enabled;


	@Override
	public void init(final InitContext initContext)
		throws Exception {

		Properties props = new Properties();
		props.load(initContext.getResourceAsStream("/config.properties"));

		enabled = props.getProperty("mockClaimsSource.enabled", "true").equals("true");
	}


	@Override
	public boolean isEnabled() {

		return enabled;
	}


	@Override
	public Set<String> supportedClaims() {

		return new HashSet<>(Arrays.asList("email", "name"));
	}


	@Override
	public UserInfo getClaims(final Subject subject,
				  final Set<String> claims,
				  final List<LangTag> claimsLocales)
		throws Exception {

		if (! enabled) {
			return null;
		}

		if (! subject.equals(new Subject("alice"))) {
			return null;
		}

		UserInfo alice = new UserInfo(subject);

		if (claims.contains("email")) {
			alice.setEmail(new InternetAddress("alice@wonderland.net"));
		}

		if (claims.contains("name")) {
			alice.setName("Alice Adams");
		}

		return alice;
	}


	@Override
	public void shutdown() {

	}
}

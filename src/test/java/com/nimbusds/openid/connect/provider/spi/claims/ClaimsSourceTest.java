package com.nimbusds.openid.connect.provider.spi.claims;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;

import junit.framework.TestCase;

import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

import com.nimbusds.openid.connect.provider.spi.MockServletContext;
import com.nimbusds.openid.connect.provider.spi.ServletInitContext;


/**
 * Tests the claims source interface.
 */
public class ClaimsSourceTest extends TestCase {


	public void testRun()
		throws Exception {

		Properties props = new Properties();
		props.setProperty("mockClaimsSource.enabled", "true");
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		props.store(stream, null);

		MockServletContext mockServletContext = new MockServletContext();
		mockServletContext.setResourceAsStream("/config.properties", new ByteArrayInputStream(stream.toByteArray()));

		ServletInitContext initContext = new ServletInitContext(mockServletContext);

		ClaimsSource source = new MockClaimsSource();
		source.init(initContext);

		assertTrue(source.isEnabled());

		assertTrue(source.supportedClaims().contains("email"));
		assertTrue(source.supportedClaims().contains("name"));
		assertEquals(2, source.supportedClaims().size());

		assertNull(source.getClaims(new Subject("no-such-user"),
			new HashSet<>(Arrays.asList("name", "email")),
			null));

		UserInfo alice = source.getClaims(new Subject("alice"),
			new HashSet<>(Arrays.asList("name", "email")),
			null);

		assertEquals("alice", alice.getSubject().getValue());
		assertEquals("Alice Adams", alice.getName());
		assertEquals("alice@wonderland.net", alice.getEmail().toString());

		source.shutdown();
	}
}
